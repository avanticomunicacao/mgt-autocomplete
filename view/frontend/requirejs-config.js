var config = {
    map: {
        '*': {
            "Magento_Ui/js/form/element/post-code": 'Avanti_AutoComplete/js/form/element/post-code',
            "addressValidation": "Avanti_AutoComplete/js/addressValidation",
            "regexCep": "Avanti_AutoComplete/js/regexCep",
            "fillAddress": "Avanti_AutoComplete/js/fillAddress",
            "ajaxRequest": "Avanti_AutoComplete/js/ajaxRequest",
        }
    }
};