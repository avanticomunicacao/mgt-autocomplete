define(['jquery', 'mage/url', 'fillAddress'], function ($, url, fillAddress) {
    return {
        requestAjax: function (zipCode) {
            url.setBaseUrl(BASE_URL);
            let ajaxUrl = url.build("autocomplete/address/index");
            $.ajax({
                showLoader: true,
                data: {
                    "zipcode": zipCode
                },
                url: ajaxUrl,
                type: "POST"
            }).done(function (addressData) {
                fillAddress.fillStreetField(addressData.logradouro);
                fillAddress.fillCityField(addressData.localidade);
                fillAddress.fillNeighborhoodField(addressData.bairro);
                fillAddress.fillRegionField(addressData.uf);
            });
        }
    }
});
