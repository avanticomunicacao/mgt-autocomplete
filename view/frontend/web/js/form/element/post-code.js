define([
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/abstract',
    'jquery',
    'regexCep',
    "ajaxRequest"
], function (_, registry, Abstract, $, regexCep, ajaxRequest) {
    'use strict';
    return Abstract.extend({
        defaults: {
            imports: {
                update: '${ $.parentName }.postcode:value'
            }
        },
        /**
         * @param {String} value
         */
        update: function (zipCode) {
            if (!zipCode || window.checkoutConfig.module_enable !== '1') {
                return;
            }
            
            if (regexCep.myFunction(zipCode) === true) {
                ajaxRequest.requestAjax(zipCode);
            }
        }
    });
});
