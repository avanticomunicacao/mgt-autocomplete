define(['jquery'], function ($) {
    return {
        fillStreetField : function (streetValue) {
            if ($('#street_1').length){
                $("#street_1").val(streetValue);
                return;
            }
            $('input[name="street[0]"]').val(streetValue);
            $('input[name="street[0]"]').change();
        },

        fillCityField: function (cityValue) {
            $('input[name="city"]').val(cityValue);
            $('input[name="city"]').change();
        },

        fillNeighborhoodField: function (neighborhoodValue) {
            if ($('#street_4').length){
                $('#street_4').val(neighborhoodValue);
                return;
            }

            $('input[name="street[3]"]').val(neighborhoodValue);
            $('input[name="street[3]"]').change();
        },

        fillRegionField: function (regionValue) {
            $('[name="region_id"]').val(regionValue);
            $('[name="region_id"]').change();
        }
    }
});
