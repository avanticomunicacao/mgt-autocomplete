define(['jquery'], function ($) {
    return {
        myFunction: function (value) {
            var regexExpression = /^[0-9]{5}-[0-9]{3}$/;
            var zipCode = this.trim(value);

            if (zipCode.length > 0) {
                if (regexExpression.test(zipCode)) {
                    return true;
                } else {
                    return false;
                }
            }
        },

        trim: function(zipCode) {
            return zipCode.replace(/^s+|s+$/g, '');
        }
    }
});