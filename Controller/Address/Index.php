<?php
namespace Avanti\AutoComplete\Controller\Address;


use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use \Magento\Framework\Controller\ResultFactory;
use Avanti\AutoComplete\Model\ViaCep;


class Index extends Action
{

    protected $viaCep;

    public function __construct(ViaCep $viaCep, Context $context)
    {
        $this->viaCep = $viaCep;
        parent::__construct($context);
    }

    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        if (!$post) {
            $this->_redirect('*/*/');
            return;
        }

        try {
            $dataPostcode = $this->viaCep->find($post['zipcode']);

            $response = $this->resultFactory
                ->create(ResultFactory::TYPE_JSON)
                ->setData([
                    'cep' => $dataPostcode['cep'],
                    'logradouro' => $dataPostcode['logradouro'],
                    'bairro' => $dataPostcode['bairro'],
                    'localidade' => $dataPostcode['localidade'],
                    'uf' => $dataPostcode['uf'],
                ]);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\NotFoundException(__('Postcode not found'));
        }
        return $response;
    }
}