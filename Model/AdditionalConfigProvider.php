<?php
namespace Avanti\AutoComplete\Model;

use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Store\Model\ScopeInterface;

class AdditionalConfigProvider
{
    private $scopeConfig;

    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    public function getConfig()
    {
        $storeScope = ScopeInterface::SCOPE_STORES;
        $value = $this->scopeConfig->getValue("avanti_autocomplete/general/enable", $storeScope);

        $output['module_enable'] = $value;
        return $output;
    }
}