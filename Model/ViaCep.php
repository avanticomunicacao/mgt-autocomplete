<?php


namespace Avanti\AutoComplete\Model;

use GuzzleHttp\Client;
use Magento\Directory\Model\RegionFactory;
use Magento\Setup\Exception;

class ViaCep
{
    private $httpRequest;
    private $regionFactory;

    const COUNTRY_ID = 'BR';

    public function __construct(Client $httpRequest, RegionFactory $regionFactory)
    {
        $this->httpRequest = $httpRequest;
        $this->regionFactory = $regionFactory;
    }

    public function find($zipcode)
    {
        try {
            $zipcode = str_replace('-', '', $zipcode);
            $response = $this->httpRequest->request("GET", "https://viacep.com.br/ws/" . $zipcode . "/json/");
            $data = json_decode($response->getBody(), true);

            $regionId = $this->findRegion($data['uf']);
            $data['uf'] = $regionId;


            if (array_key_exists('erro', $data) && $data['erro'] === true) {
                throw new \Magento\Framework\Exception\NotFoundException(__('Postcode not found'));
            }
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\NotFoundException(__('RegionId not found'));
        }

        return $data;
    }

    private function findRegion($id)
    {
        try {
            $region = $this->regionFactory->create();
            return $region->loadByCode($id, self::COUNTRY_ID)->getId();
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\NotFoundException(__('Region not found'));
        }
    }
}